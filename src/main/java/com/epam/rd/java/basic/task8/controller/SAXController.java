package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.Monitor;
import com.epam.rd.java.basic.task8.entity.Part;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private String xmlFileName;

	private String currentElement;

	private Monitor monitor;

	private Part part;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	/**
	 * Parses XML document.
	 *
	 * @param validate If true validate XML document against its XML schema. With
	 *                 this parameter it is possible make parser validating.
	 */
	public void parse(boolean validate) throws SAXException, ParserConfigurationException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();

		factory.setNamespaceAware(true);

		if (validate) {
			factory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
			factory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName, this);
	}

	// ///////////////////////////////////////////////////////////
	// ERROR HANDLER IMPLEMENTATION
	// ///////////////////////////////////////////////////////////

	@Override
	public void error(org.xml.sax.SAXParseException e) throws SAXException {
		// if XML document not valid just throw exception
		throw e;
	}

	public Monitor getMonitor() {
		return monitor;
	}

	// ///////////////////////////////////////////////////////////
	// CONTENT HANDLER IMPLEMENTATION
	// ///////////////////////////////////////////////////////////

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		currentElement = localName;

		if (XML.MONITOR.equalsTo(currentElement)) {
			monitor = new Monitor();
			return;
		}

		if (XML.PART.equalsTo(currentElement)) {
			part = new Part();
			if(attributes.getLength() > 0) {
				part.setAffordable(Boolean.parseBoolean(attributes.getValue(uri,
						XML.AFFORDABLE.value())));
			}
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {

		String elementText = new String(ch, start, length).trim();

		// return if content is empty
		if (elementText.isEmpty()) {
			return;
		}

		if (XML.NAME.equalsTo(currentElement)) {
			part.setName(elementText);
			return;
		}

		if (XML.ORIGIN.equalsTo(currentElement)) {
			part.setOrigin(elementText);
			return;
		}

		if (XML.PRICE.equalsTo(currentElement)) {
			part.setPrice(Integer.valueOf(elementText));
			return;
		}

		if (XML.MATRIX.equalsTo(currentElement)) {
			part.setMatrix(elementText);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		if (XML.PART.equalsTo(localName)) {
			// just add question to container
			monitor.getParts().add(part);
		}
	}

	public static void main(String[] args) throws Exception {

		// try to parse valid XML file (success)
		SAXController saxContr = new SAXController(Constants.VALID_XML_FILE);

		// do parse with validation on (success)
		saxContr.parse(true);

		// obtain container
		Monitor monitor = saxContr.getMonitor();

		// we have Monitor object at this point:
		System.out.println("===================================");
		System.out.print("Here is the Monitor: \n" + monitor);
		System.out.println("====================================");

		// now try to parse NOT valid XML (failed)
		saxContr = new SAXController(Constants.INVALID_XML_FILE);
		try {
			// do parse with validation on (failed)
			saxContr.parse(true);
		} catch (Exception ex) {
			System.err.println("=====================================");
			System.err.println("Validation is failed:\n" + ex.getMessage());
			System.err
					.println("Try to print Monitor object:" + saxContr.getMonitor());
			System.err.println("=======================================");
		}

		// and now try to parse NOT valid XML with validation off (success)
		saxContr.parse(false);

		// we have Monitor object at this point:
		System.out.println("==================================");
		System.out.print("Here is the Monitor: \n" + saxContr.getMonitor());
		System.out.println("======================================");
	}

}