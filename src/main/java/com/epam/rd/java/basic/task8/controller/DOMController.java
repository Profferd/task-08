package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.Monitor;
import com.epam.rd.java.basic.task8.entity.Part;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
/**
 * Controller for DOM parser.
 *
 */
public class DOMController {

	private String xmlFileName;

	// main container
	private Monitor monitor;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Monitor getMonitor() {
		return monitor;
	}

	/**
	 * Parses XML document.
	 *
	 * @param validate If true validate XML document against its XML schema.
	 */
	public void parse(boolean validate) throws ParserConfigurationException, SAXException, IOException {

		// obtain DOM parser
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, false);

		// set properties for Factory

		// XML document contains namespaces
		dbf.setNamespaceAware(true);

		// make parser validating
		if (validate) {
			// turn validation on
			dbf.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);

			// turn on xsd validation
			dbf.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		DocumentBuilder db = dbf.newDocumentBuilder();

		// set error handler
		db.setErrorHandler(new DefaultHandler() {
			@Override
			public void error(SAXParseException e) throws SAXException {
				// throw exception if XML document is NOT valid
				throw e;
			}
		});

		// parse XML document
		Document document = db.parse(xmlFileName);

		// get root element
		Element root = document.getDocumentElement();

		// create container
		monitor = new Monitor();

		// obtain parts nodes
		NodeList partNodes = root.getElementsByTagName(XML.PART.value());

		// process parts nodes
		for (int j = 0; j < partNodes.getLength(); j++) {
			Part part = getPart(partNodes.item(j));
			// add part to container
			monitor.getParts().add(part);
		}
	}

	/**
	 * Extracts part object from the part XML node.
	 *
	 * @param qNode Part node.
	 * @return Part object.
	 */
	private static Part getPart(Node qNode) {
		Part part = new Part();
		Element qElement = (Element) qNode;
		// process part text
		Node qtNode = qElement.getElementsByTagName(XML.NAME.value()).item(0);
		part.setName(qtNode.getTextContent());
		qtNode = qElement.getElementsByTagName(XML.ORIGIN.value()).item(0);
		part.setOrigin(qtNode.getTextContent());
		qtNode = qElement.getElementsByTagName(XML.PRICE.value()).item(0);
		part.setPrice(Integer.parseInt(qtNode.getTextContent()));
		qtNode = qElement.getElementsByTagName(XML.MATRIX.value()).item(0);
		// set part text
		part.setMatrix(qtNode.getTextContent());
		String correct = qElement.getAttribute(XML.AFFORDABLE.value());
		part.setAffordable(Boolean.valueOf(correct));

		return part;
	}

	// //////////////////////////////////////////////////////
	// Static util methods
	// //////////////////////////////////////////////////////

	/**
	 * Creates and returns DOM of the Monitor container.
	 *
	 * @param monitor Monitor object.
	 * @throws ParserConfigurationException
	 */
	public static Document getDocument(Monitor monitor) throws ParserConfigurationException {

		// obtain DOM parser
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		// set properties for Factory

		// XML document contains namespaces
		dbf.setNamespaceAware(true);

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		// create root element
		Element tElement = document.createElement(XML.MONITOR.value());

		// add root element
		document.appendChild(tElement);

		// add parts elements
		for (Part part : monitor.getParts()) {

			// add part
			Element qElement = document.createElement(XML.PART.value());
			tElement.appendChild(qElement);
			if (part.isAffordable()) {
				qElement.setAttribute(XML.AFFORDABLE.value(), "true");
			}
			// add part text
			Element qtElement = document.createElement(XML.NAME.value());

			qElement.appendChild(qtElement);

			qtElement.setTextContent(part.getName());
			qElement.appendChild(qtElement);
			qtElement = document.createElement(XML.ORIGIN.value());
			qtElement.setTextContent(part.getOrigin());
			qElement.appendChild(qtElement);
			qtElement = document.createElement(XML.PRICE.value());
			qtElement.setTextContent(String.valueOf(part.getPrice()));
			qElement.appendChild(qtElement);
			qtElement = document.createElement(XML.MATRIX.value());
			qtElement.setTextContent(part.getMatrix());
			qElement.appendChild(qtElement);

			// set attribute

		}

		return document;
	}

	/**
	 * Saves Monitor object to XML file.
	 *
	 * @param monitor        Monitor object to be saved.
	 * @param xmlFileName Output XML file name.
	 */
	public static void saveToXML(Monitor monitor, String xmlFileName)
			throws ParserConfigurationException, TransformerException {
		// Monitor -> DOM -> XML
		saveToXML(getDocument(monitor), xmlFileName);
	}

	/**
	 * Save DOM to XML.
	 *
	 * @param document    DOM to be saved.
	 * @param xmlFileName Output XML file name.
	 */
	public static void saveToXML(Document document, String xmlFileName) throws TransformerException {

		StreamResult result = new StreamResult(new File(xmlFileName));

		// set up transformation
		TransformerFactory tf = TransformerFactory.newInstance();
		tf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, false);
		javax.xml.transform.Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");

		// run transformation
		t.transform(new DOMSource(document), result);
	}

}